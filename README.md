# GS1 Web Vocabulary

The GS1 web vocabulary provides a collection of schemas that can be used to help add structured data to HTML pages.  It:

* is based on the GS1 Trade Item and Party models in the GS1 Global Data Dictionary and thus improves interoperability with existing GS1 Standards
* is written in Terse RDF Triple Language (Turtle)

The links below provides a browseable view.

* [Address](http://www.gs1.org/voc/Address)
* [AwardPrize](http://www.gs1.org/voc/AwardPrize)
* [Beverage](http://www.gs1.org/voc/Beverage)
* [Certification](http://www.gs1.org/voc/Certification)
* [Contact](http://www.gs1.org/voc/Contact)
* [Clothing](http://www.gs1.org/voc/Clothing)
* [FoodAndBeveragePreparationInformation](http://www.gs1.org/voc/FoodAndBeveragePreparationInformation)
* [FoodBeverageTobaccoIngredient](http://www.gs1.org/voc/FoodBeverageTobaccoIngredient)
* [FoodBeverageTobaccoTradeItem](http://www.gs1.org/voc/FoodBeverageTobaccoTradeItem)
* [Footwear](http://www.gs1.org/voc/Footwear)
* [FruitsVegetables](http://www.gs1.org/voc/FruitsVegetables)
* [MeatPoultry](http://www.gs1.org/voc/MeatPoultry)
* [MilkButterCreamYogurtCheeseEggsSubstitutes](http://www.gs1.org/voc/MilkButterCreamYogurtCheeseEggsSubstitutes)
* [Offering](http://www.gs1.org/voc/Offering)
* [Packaging](http://www.gs1.org/voc/Packaging)
* [Price](http://www.gs1.org/voc/Price)
* [ReferencedFile](http://www.gs1.org/voc/ReferencedFile)
* [Seafood](http://www.gs1.org/voc/Seafood)
* [TextileMaterial](http://www.gs1.org/voc/TextileMaterial)
* [TradeItem](http://www.gs1.org/voc/TradeItem)
* [Warranty](http://www.gs1.org/voc/Warranty)
* [WearableTradeItem](http://www.gs1.org/voc/WearableTradeItem)

## Download

You can download the vocabulary in various formats:

* [Turtle](http://dydra.com/nlv01111/gs1.ttl)
* [N-Triples](http://dydra.com/nlv01111/gs1.nt)
* [N-Quads](http://dydra.com/nlv01111/gs1.nq)
* [JSON-LD](http://dydra.com/nlv01111/gs1.jsonld)
* [RDF/JSON](http://dydra.com/nlv01111/gs1.rj)
* [RDF/XML](http://dydra.com/nlv01111/gs1.rdf)

## SPARQL endpoint

You can query the SPARQL endpoint for the vocabulary here:

[http://dydra.com/nlv01111/gs1/sparql](http://dydra.com/nlv01111/gs1/sparql)

The core vocabulary from file `gs1vocab_v1.ttl` has been loaded to the default graph. The code lists are each loaded to distinct named graph with IRI template `http://graph.gs1.org/{name}` where `name` is the filename of the source Turtle file without the .ttl extension, for example, the file CheeseFirmnessCode.ttl is loaded to graph `http://graph.gs1.org/CheeseFirmnessCode`.