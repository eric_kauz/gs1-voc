echo Please enter your Dydra credentials
echo ===================================
set /p username="Enter your username:"
set /p password="Enter your password:"
FOR %%X IN (*.ttl) DO (
	if "%%X" == "gs1vocab_v1.ttl" (
		curl -s -w %%{http_code}\n -X PUT -H "Content-Type: text/turtle" --data-binary @%%X http://%username%:%password%@dydra.com/nlv01111/gs1/service?default
	) else (
		curl -s -w %%{http_code}\n -X PUT -H "Content-Type: text/turtle" --data-binary @%%X http://%username%:%password%@dydra.com/nlv01111/gs1/service?graph=http%%3A%%2F%%2Fgraph.gs1.org%%2F%%~nX
	)
)
pause